import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class ToDoListServiceService {

  constructor() { }

  getTodos() {
    return axios.get('http://demo4180697.mockable.io/todos');
  }
}
