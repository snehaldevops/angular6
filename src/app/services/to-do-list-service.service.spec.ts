import { TestBed, inject } from '@angular/core/testing';

import { ToDoListServiceService } from './to-do-list-service.service';

describe('ToDoListServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToDoListServiceService]
    });
  });

  it('should be created', inject([ToDoListServiceService], (service: ToDoListServiceService) => {
    expect(service).toBeTruthy();
  }));
});
