import { Component, OnInit } from '@angular/core';
import {ToDoListServiceService} from '../services/to-do-list-service.service';




@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.scss']
})
export class TodolistComponent implements OnInit {
  items: any[];
  newItem: string;


  constructor(private toDoListServiceService: ToDoListServiceService ) {

  }

  ngOnInit() {
    this.newItem = 'snehal' ;

    this.toDoListServiceService.getTodos()
        .then( (response) => {
          this.items = response.data.todo;
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
  }

  addItemToList() {

    if (this.newItem.trim() !== '') {
      this.items.push(this.newItem);
      this.newItem = '' ;
    }
  }


  removeItem(index) {
    this.items.splice( index , 1);
  }

}
